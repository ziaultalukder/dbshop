﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DbShop.BLL.Contracts;
using DbShop.Models.EntityModel;
using DbShop.ViewModel;

namespace DbShop.Controllers
{
    public class NewCollectionController : Controller
    {
        // GET: NewCollection
        public INewCollectionManager NewCollectionManager;

        public NewCollectionController(INewCollectionManager Manager)
        {
            NewCollectionManager = Manager;
        }
        public ActionResult Index()
        {
            var newCollection = NewCollectionManager.GetAll();
            List<NewCollectionViewModel> newCollectionViewModels = new List<NewCollectionViewModel>();
            foreach (var item in newCollection)
            {
                newCollectionViewModels.Add(new NewCollectionViewModel()
                {
                    Id = item.Id,
                    Title = item.Title,
                    ImagePath = item.ImagePath
                });
            }
            return View(newCollectionViewModels);
        }

        // GET: NewCollection/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var newCollection = NewCollectionManager.GetById((int) id);
            NewCollectionViewModel newCollectionViewModel = new NewCollectionViewModel()
            {
                Id = newCollection.Id,
                Title = newCollection.Title,
                ImagePath = newCollection.ImagePath
            };
            return View(newCollectionViewModel);
        }

        // GET: NewCollection/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NewCollection/Create
        [HttpPost]
        public ActionResult Create(NewCollectionViewModel newCollectionViewModel)
        {
            try
            {
                string fileName = Path.GetFileNameWithoutExtension(newCollectionViewModel.Image.FileName);
                string extension = Path.GetExtension(newCollectionViewModel.Image.FileName);
                var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                fileName = Path.Combine(Server.MapPath("~/NewCollectionImage/"), fileNames);
                newCollectionViewModel.Image.SaveAs(fileName);

                NewCollection newCollection = new NewCollection()
                {
                    Title = newCollectionViewModel.Title,
                    ImagePath = path
                };
                NewCollectionManager.Add(newCollection);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: NewCollection/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var newCollection = NewCollectionManager.GetById((int)id);
            NewCollectionViewModel newCollectionViewModel = new NewCollectionViewModel()
            {
                Id = newCollection.Id,
                Title = newCollection.Title,
                ImagePath = newCollection.ImagePath
            };
            return View(newCollectionViewModel);
        }

        // POST: NewCollection/Edit/5
        [HttpPost]
        public ActionResult Edit(NewCollectionViewModel newCollectionViewModel)
        {
            try
            {
                if (newCollectionViewModel.Image != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(newCollectionViewModel.Image.FileName);
                    string extension = Path.GetExtension(newCollectionViewModel.Image.FileName);
                    var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    fileName = Path.Combine(Server.MapPath("~/NewCollectionImage/"), fileNames);
                    newCollectionViewModel.Image.SaveAs(fileName);

                    NewCollection newCollection = new NewCollection()
                    {
                        Id = newCollectionViewModel.Id,
                        Title = newCollectionViewModel.Title,
                        ImagePath = path
                    };
                    NewCollectionManager.Update(newCollection);
                }
                else
                {
                    NewCollection newCollection = new NewCollection()
                    {
                        Id = newCollectionViewModel.Id,
                        Title = newCollectionViewModel.Title,
                        ImagePath = newCollectionViewModel.ImagePath
                    };
                    NewCollectionManager.Update(newCollection);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: NewCollection/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var newCollection = NewCollectionManager.GetById((int)id);
            NewCollectionManager.Remove(newCollection);
            return RedirectToAction("Index");
        }
    }
}
