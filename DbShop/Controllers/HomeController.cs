﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DbShop.BLL.Contracts;
using DbShop.ViewModel;

namespace DbShop.Controllers
{
    public class HomeController : Controller
    {
        public ICategoryManager categoryManager;
        public IBannerImageManager BannerImageManager;
        public INewCollectionManager NewCollectionManager;
        public IProductImageManager ProductImageManager;
        public IProductManager ProductManager;

        public HomeController(ICategoryManager category, IBannerImageManager BannerImage,
            INewCollectionManager Manager, IProductImageManager ProductImage, IProductManager Product)
        {
            categoryManager = category;
            BannerImageManager = BannerImage;
            NewCollectionManager = Manager;
            ProductImageManager = ProductImage;
            ProductManager = Product;
        }
        public ActionResult Index()
        {
            var category = categoryManager.GetAll();
            var bannerImage = BannerImageManager.GetAll().OrderByDescending(c=>c.Id).Take(3);
            var product = ProductManager.GetAll();
            var productImage = ProductImageManager.GetAll();

            var firstNewCollection1 = NewCollectionManager.GetAll().OrderByDescending(c => c.Id).Take(1);
            var SecondNewCollection2 = NewCollectionManager.GetAll().OrderByDescending(c => c.Id).Skip(1).Take(1);
            var ThirdNewCollection3 = NewCollectionManager.GetAll().OrderByDescending(c => c.Id).Skip(2).Take(1);
            var FourthNewCollection4 = NewCollectionManager.GetAll().OrderByDescending(c => c.Id).Skip(3).Take(1);
            var FiveNewCollection4 = NewCollectionManager.GetAll().OrderByDescending(c => c.Id).Skip(4).Take(1);


            List<HomeProductViewModel> homeProductViewModels = new List<HomeProductViewModel>();
            foreach (var ProductDetails in product)
            {
                var productImages = productImage.FirstOrDefault(c => c.ProductId == ProductDetails.Id);
                HomeProductViewModel homeView = new HomeProductViewModel()
                {
                    Id = ProductDetails.Id,
                    ProductName = ProductDetails.Name,
                    ProductPrice = ProductDetails.Price,
                    ImagePath = productImages.ImagePath
                };
                homeProductViewModels.Add(homeView);
            }
            HomeViewModel homeViewModel = new HomeViewModel()
            {
                
                Categories = category,
                BannerImages = bannerImage,
                NewCollections1 = firstNewCollection1,
                NewCollections2 = SecondNewCollection2,
                NewCollections3 = ThirdNewCollection3,
                NewCollections4 = FourthNewCollection4,
                NewCollections5 = FiveNewCollection4,
                HomeProductViewModels = homeProductViewModels
            };
            return View(homeViewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CategoryDropDown(int? id)
        {
            return View();
        }
    }
}