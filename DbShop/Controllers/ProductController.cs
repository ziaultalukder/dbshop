﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using DbShop.BLL.Contracts;
using DbShop.Models.EntityModel;
using DbShop.ViewModel;
using Microsoft.AspNet.Identity;

namespace DbShop.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        // GET: Product
        public IProductManager ProductManager;
        public ICategoryManager CategoryManager;
        public IProductImageManager ProductImageManager;

        public ProductController(IProductManager manager, ICategoryManager Category, IProductImageManager productImage)
        {
            ProductManager = manager;
            CategoryManager = Category;
            ProductImageManager = productImage;
        }
        public ActionResult Index()
        {
            var category = CategoryManager.GetAll();
            var Product = ProductManager.GetAll();
            var productImages = ProductImageManager.GetAll();
            //var size = GetSizeList();


            List<ProductViewModel> productViewModels = new List<ProductViewModel>();
            foreach (var data in Product)
            {
                var productImage = productImages.FirstOrDefault(c => c.ProductId == data.Id);
                var productVM = new ProductViewModel()
                {
                    Id = data.Id,
                    Name = data.Name,
                    Price = data.Price,
                    Description = data.Description,
                    UserName = data.UserName,
                    Details = data.Details,
                    Brand = data.Brand,
                    Category = category.FirstOrDefault(c=>c.Id == data.CategoryId),
                    FilePath = productImage.ImagePath
                };
                productViewModels.Add(productVM);
            }
            return View(productViewModels);
        }

        // GET: Product/Details/5
        public ActionResult Details(int? id)
        {
            var category = CategoryManager.GetAll();
            var productImage = ProductImageManager.GetAll();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = ProductManager.GetById((int) id);
            if (product == null)
            {
                return HttpNotFound();
            }
            var productImageList = productImage.Where(c => c.ProductId == product.Id);
            ProductViewModel productViewModel = new ProductViewModel()
            {
                Id = product.Id,
                Name = product.Name,
                Brand = product.Brand,
                Category = category.FirstOrDefault(c=>c.Id == product.CategoryId),
                Details = product.Details,
                Price = product.Price,
                Description = product.Description,
                UserName = product.UserName,
                ProductImages = productImageList
            };
            return View(productViewModel);
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            var category = CategoryManager.GetAll();
            ProductViewModel productViewModel = new ProductViewModel();
            productViewModel.Categories = category;
            return View(productViewModel);
        }


        // POST: Product/Create
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ProductViewModel productViewModel)
        {
            try
            {
                var userName = User.Identity.GetUserName();

                List<ProductImage> productImages = new List<ProductImage>();
                List<string> FilePath = new List<string>();
                foreach (var data in productViewModel.Image)
                {
                    string fileName = Path.GetFileNameWithoutExtension(data.FileName);
                    string extension = Path.GetExtension(data.FileName);
                    var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    fileName = Path.Combine(Server.MapPath("~/ProductImage/"), fileNames);
                    data.SaveAs(fileName);
                    FilePath.Add(path);
                }
                foreach (var data in FilePath)
                {
                    productImages.Add(new ProductImage()
                    {
                        ImagePath = data
                    });
                }

                Product product = new Product()
                {
                    Name = productViewModel.Name,
                    Brand = productViewModel.Brand,
                    CategoryId = productViewModel.CategoryId,
                    Description = productViewModel.Description,
                    Details = productViewModel.Details,
                    Price = productViewModel.Price,
                    UserName = userName,
                    ProductImages = productImages
                };
                ProductManager.Add(product);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = ProductManager.GetById((int) id);
            var productImage = ProductImageManager.GetAll();
            var deleteProductImage = productImage.Where(c => c.ProductId == product.Id);


            ProductViewModel productViewModel = new ProductViewModel()
            {
                Id = product.Id,
                Brand = product.Brand,
                Name = product.Name,
                Description = product.Description,
                Details = product.Details,
                CategoryId = product.CategoryId,
                ProductImages = deleteProductImage,
                Price = product.Price
            };
            ViewBag.CategoryId = new SelectList(CategoryManager.GetAll(), "Id","Name", product.CategoryId);
            return View(productViewModel);
        }

        // POST: Product/Edit/5
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(ProductViewModel productViewModel)
        {
            try
            {
                List<ProductImage> productImages = new List<ProductImage>();
                List<string> FilePath = new List<string>();
                var prd = productViewModel.Image;
                
                var datas = ProductImageManager.GetAll();
                var imageData = datas.Where(c => c.ProductId == productViewModel.Id);

                if (productViewModel.Image != null)
                {
                    foreach (var data in productViewModel.Image)
                    {
                        if (data == null)
                        {

                        }
                        string fileName = Path.GetFileNameWithoutExtension(data.FileName);
                        string extension = Path.GetExtension(data.FileName);
                        var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                        string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                        fileName = Path.Combine(Server.MapPath("~/ProductImage/"), fileNames);
                        data.SaveAs(fileName);
                        FilePath.Add(path);
                    }

                    
                }

                if (productViewModel.Image == null)
                {
                    Product product = new Product()
                    {
                        Name = productViewModel.Name,
                        Brand = productViewModel.Brand,
                        CategoryId = productViewModel.CategoryId,
                        Description = productViewModel.Description,
                        Details = productViewModel.Details,
                        Price = productViewModel.Price,
                        //ProductImages = productImages
                    };
                }


                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = ProductManager.GetById((int)id);
            var productImage = ProductImageManager.GetAll();
            var deleteProductImage = productImage.Where(c => c.ProductId == product.Id);

            ProductManager.Remove(product);
            foreach (var image in deleteProductImage)
            {
                ProductImageManager.Remove(image);
            }

            return RedirectToAction("Index");
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
