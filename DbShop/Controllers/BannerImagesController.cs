﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DbShop.BLL.Contracts;
using DbShop.Models.EntityModel;
using DbShop.ViewModel;

namespace DbShop.Controllers
{
    public class BannerImagesController : Controller
    {
        public IBannerImageManager BannerImageManager;

        public BannerImagesController(IBannerImageManager Manager)
        {
            BannerImageManager = Manager;
        }
        // GET: SliderImage
        public ActionResult Index()
        {
            var bannerImage = BannerImageManager.GetAll();
            List<BannerImageViewModel> bannerImageViewModels = new List<BannerImageViewModel>();
            foreach (var data in bannerImage)
            {
                bannerImageViewModels.Add(new BannerImageViewModel()
                {
                    Id = data.Id,
                    Title = data.Title,
                    Discount = data.Discount,
                    ImagePath = data.ImagePath
                });
            }
            return View(bannerImageViewModels);
        }

        // GET: BannerImage/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var bannerImage = BannerImageManager.GetById((int)id);
            BannerImageViewModel bannerImageViewModel = new BannerImageViewModel()
            {
                Id = bannerImage.Id,
                Title = bannerImage.Title,
                Discount = bannerImage.Discount,
                ImagePath = bannerImage.ImagePath
            };
            return View(bannerImageViewModel);
        }

        // GET: BannerImage/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BannerImage/Create
        [HttpPost]
        public ActionResult Create(BannerImageViewModel bannerImageViewModel)
        {
            try
            {
                string fileName = Path.GetFileNameWithoutExtension(bannerImageViewModel.Image.FileName);
                string extension = Path.GetExtension(bannerImageViewModel.Image.FileName);
                var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                fileName = Path.Combine(Server.MapPath("~/BannerImage/"), fileNames);
                bannerImageViewModel.Image.SaveAs(fileName);

                BannerImage bannerImage = new BannerImage()
                {
                    Title = bannerImageViewModel.Title,
                    Discount = bannerImageViewModel.Discount,
                    ImagePath = path,
                };
                BannerImageManager.Add(bannerImage);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BannerImage/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var bannerImage = BannerImageManager.GetById((int)id);
            BannerImageViewModel bannerImageViewModel = new BannerImageViewModel()
            {
                Id = bannerImage.Id,
                Title = bannerImage.Title,
                Discount = bannerImage.Discount,
                ImagePath = bannerImage.ImagePath
            };
            return View(bannerImageViewModel);
        }

        // POST: BannerImage/Edit/5
        [HttpPost]
        public ActionResult Edit(BannerImageViewModel bannerImageViewModel)
        {
            try
            {
                if (bannerImageViewModel.Image != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(bannerImageViewModel.Image.FileName);
                    string extension = Path.GetExtension(bannerImageViewModel.Image.FileName);
                    var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    fileName = Path.Combine(Server.MapPath("~/BannerImage/"), fileNames);
                    bannerImageViewModel.Image.SaveAs(fileName);
                    BannerImage bannerImage = new BannerImage()
                    {
                        Id = bannerImageViewModel.Id,
                        Title = bannerImageViewModel.Title,
                        Discount = bannerImageViewModel.Discount,
                        ImagePath = path,
                    };
                    BannerImageManager.Update(bannerImage);
                }
                else
                {
                    BannerImage bannerImage = new BannerImage()
                    {
                        Id = bannerImageViewModel.Id,
                        Title = bannerImageViewModel.Title,
                        Discount = bannerImageViewModel.Discount,
                        ImagePath = bannerImageViewModel.ImagePath,
                    };
                    BannerImageManager.Update(bannerImage);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BannerImage/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var bannerImage = BannerImageManager.GetById((int)id);
            BannerImageViewModel bannerImageViewModel = new BannerImageViewModel()
            {
                Id = bannerImage.Id,
                Title = bannerImage.Title,
                Discount = bannerImage.Discount,
                ImagePath = bannerImage.ImagePath
            };
            var bannerImageDelete = BannerImageManager.Remove(bannerImage);
            if (bannerImageDelete)
            {
                return RedirectToAction("Index");
            }
            return View(bannerImageViewModel);
        }

        // POST: BannerImage/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}