﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DbShop.BLL.Contracts;
using DbShop.Models.EntityModel;
using DbShop.PartialClass;
using DbShop.ViewModel;
using ShopingCart.ViewModel;

namespace DbShop.Controllers
{
    public class ShoppingCartController : Controller
    {
        private ICartManager cartManager;
        private IProductManager productManager;
        private IProductImageManager productImageManager;

        public ShoppingCartController(ICartManager cartManager, IProductManager Manager, IProductImageManager productImage)
        {
            cartManager = this.cartManager;
            productManager = Manager;
            productImageManager = productImage;
        }
        // GET: ShoppingCart
        public ActionResult Index()
        {
            var cart = ShoppingCartPartial.GetCart(this.HttpContext);
            // Set up our ViewModel
            var viewModel = new ShoppingCartViewModel
            {
                CartTotal = cart.GetTotal(),
                CartItems = cart.GetCartItems()
            };
            // Return the view
            return View(viewModel);
        }

        // GET: /Store/AddToCart/5
        public ActionResult AddToCart(int id)
        {
            // Retrieve the album from the database
            var products = productManager.GetAll();
            var addedProduct = products.Single(c => c.Id == id);

            // Add it to the shopping cart
            var cart = ShoppingCart.GetCart(this.HttpContext);

            cart.AddToCart(addedProduct);

            // Go back to the main store page for more shopping
            return RedirectToAction("Index");
        }
    }
}