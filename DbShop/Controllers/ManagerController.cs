﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DbShop.BLL.Contracts;
using Microsoft.AspNet.Identity;

namespace DbShop.Controllers
{
    public class ManagerController : Controller
    {
        // GET: Manager
        public IEmployeeManager EmployeeManager;

        public ManagerController(IEmployeeManager Employee)
        {
            EmployeeManager = Employee;
        }
        public ActionResult Index()
        {
            //var employee = EmployeeManager.GetAll();
            //var userId = User.Identity.GetUserId();
            //var singleEmployee = employee.FirstOrDefault(c => c.UserId == userId);
            return View();
        }
    }
}