﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DbShop.BLL.IdentityConfig;
using DbShop.Models.IdentityModel;
using DbShop.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace DbShop.Controllers
{
    public class RoleController : Controller
    {
        // GET: Role
        public ApplicationRoleManager _roleManager;
        public RoleController()
        {

        }
        public RoleController(ApplicationRoleManager roleManager)
        {
            RoleManager = roleManager;
        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        public ActionResult Index()
        {
            var role = RoleManager.Roles.ToList();
            List<RoleViewModel> list = new List<RoleViewModel>();
            foreach (var applicationRole in role)
            {
                list.Add(new RoleViewModel() { Id = applicationRole.Id, Name = applicationRole.Name });
            }
            return View(list);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(RoleViewModel roleViewModel)
        {
            var role = new ApplicationRole() {Name = roleViewModel.Name};
            RoleManager.Create(role);
            return RedirectToAction("Create");
        }

        public async Task<ActionResult> Edit(string id)
        {
            var role = RoleManager.FindById(id);
            RoleViewModel roleViewModel = new RoleViewModel()
            {
                Id = role.Id,
                Name = role.Name
            };
            return View(roleViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(RoleViewModel roleViewModel)
        {
            ApplicationRole updatedRole = new ApplicationRole()
            {
                Id = roleViewModel.Id,
                Name = roleViewModel.Name
            };
            var result = RoleManager.Update(updatedRole);
            if (result.Succeeded)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public async Task<ActionResult> Delete(string id)
        {
            var role = RoleManager.FindById(id);
            var result = RoleManager.Delete(role);
            if (result.Succeeded)
            {
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}