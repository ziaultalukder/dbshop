﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DbShop.BLL.Contracts;
using DbShop.Models.EntityModel;
using DbShop.ViewModel;

namespace DbShop.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ICategoryManager _CategoryManager;

        public CategoryController(ICategoryManager manager)
        {
            _CategoryManager = manager;
        }
        public ActionResult Index()
        {
            var category = _CategoryManager.GetAll();
            List<CategoryViewModel> categoryViewModels = new List<CategoryViewModel>();
            foreach (var data in category)
            {
                var categoryVM = new CategoryViewModel()
                {
                    Id = data.Id,
                    Name = data.Name,
                    Cat = category.FirstOrDefault(c=>c.Id == data.CatId)
                };
                categoryViewModels.Add(categoryVM);
            }
            return View(categoryViewModels);
        }

        public ActionResult Create()
        {
            var categoryList = _CategoryManager.GetAll();
            CategoryViewModel categoryViewModel = new CategoryViewModel();
            categoryViewModel.Categories = categoryList;
            return View(categoryViewModel);
        }

        [HttpPost]
        public ActionResult Create(CategoryViewModel categoryViewModel)
        {
            try
            {
                Category category = new Category()
                {
                    Name = categoryViewModel.Name,
                    CatId = categoryViewModel.CatId
                };
                _CategoryManager.Add(category);
                return RedirectToAction("Index");
            }
            catch (Exception Ex)
            {
                return View(Ex.Message);
            }
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var category = _CategoryManager.GetById((int)id);
            if (category == null)
            {
                return HttpNotFound();
            }
            CategoryViewModel categoryViewModel = new CategoryViewModel()
            {
                Id = category.Id,
                Name = category.Name,
                CatId = category.CatId
            };
            ViewBag.CatId = new SelectList(_CategoryManager.GetAll(), "Id", "Name", category.CatId);
            return View(categoryViewModel);
        }

        [HttpPost]
        public ActionResult Edit(CategoryViewModel categoryViewModel)
        {
            try
            {
                Category category = new Category()
                {
                    Id = categoryViewModel.Id,
                    Name = categoryViewModel.Name,
                    CatId = categoryViewModel.CatId
                };
                _CategoryManager.Update(category);
                return RedirectToAction("Index");
            }
            catch (Exception Ex)
            {
                return View(Ex.Message);
            }
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var category = _CategoryManager.GetById((int)id);
            if (category == null)
            {
                return HttpNotFound();
            }
            _CategoryManager.Remove(category);
            return RedirectToAction("Index");
        }
    }
}