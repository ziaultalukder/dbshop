﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DbShop.BLL.Contracts;
using DbShop.Models.EntityModel;
using DbShop.ViewModel;
using DbShop.Models.IdentityConfig;
using DbShop.Models.IdentityModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace DbShop.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        private IEmployeeManager employeeManager;
        public ApplicationUserManager ApplicationUserManager { get; set; }

        public ApplicationUserManager Manager { get; set; }

        public ApplicationUserManager UserManager
        {
            get
            {
                return Manager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }


        public EmployeeController(IEmployeeManager Manager )
        {
            employeeManager = Manager;
        }
        public ActionResult Index()
        {
            var Employee = employeeManager.GetAll();
            List<EmployeeViewModel> employeeViewModels = new List<EmployeeViewModel>();
            foreach (var data in Employee)
            {
                var EmployeeVM = new EmployeeViewModel()
                {
                    Id = data.Id,
                    Name = data.Name,
                    Email = data.Email,
                    ContactNo = data.ContactNo,
                    Address = data.Address,
                    FilePath = data.FilePath,
                };
                employeeViewModels.Add(EmployeeVM);
            }
            return View(employeeViewModels);
        }

        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(EmployeeViewModel employeeViewModel)
        {
            try
            {
                
                string fileName = Path.GetFileNameWithoutExtension(employeeViewModel.Image.FileName);
                string extension = Path.GetExtension(employeeViewModel.Image.FileName);
                var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                fileName = Path.Combine(Server.MapPath("~/EmployeeImage/"), fileNames);
                employeeViewModel.Image.SaveAs(fileName);

                ApplicationUser user = new ApplicationUser()
                {
                    Email = employeeViewModel.Email,
                    UserName = employeeViewModel.Email,
                    FilePath = path,
                    FullName = employeeViewModel.Name
                };
                string defaultPassword = "Zi@123456";
                var result = UserManager.Create(user, defaultPassword);
                if (result.Succeeded)
                {
                    var role = UserManager.AddToRole(user.Id, "Editor");
                    var results = role.Succeeded;
                }

                Employee employee = new Employee()
                {
                    Name = employeeViewModel.Name,
                    ContactNo = employeeViewModel.ContactNo,
                    Email = employeeViewModel.Email,
                    Address = employeeViewModel.Address,
                    FilePath = path,
                    UserId = user.Id,
                };

                employeeManager.Add(employee);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var employee = employeeManager.GetById((int)id);
            EmployeeViewModel employeeViewModel = new EmployeeViewModel()
            {
                Id = employee.Id,
                Name = employee.Name,
                Email = employee.Email,
                ContactNo = employee.ContactNo,
                Address = employee.Address,
                FilePath = employee.FilePath
            };
            return View(employeeViewModel);
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(EmployeeViewModel employeeViewModel)
        {
            try
            {
                if (employeeViewModel.Image != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(employeeViewModel.Image.FileName);
                    string extension = Path.GetExtension(employeeViewModel.Image.FileName);
                    var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    fileName = Path.Combine(Server.MapPath("~/EmployeeImage/"), fileNames);
                    employeeViewModel.Image.SaveAs(fileName);

                    Employee employee = new Employee()
                    {
                        Id = employeeViewModel.Id,
                        Name = employeeViewModel.Name,
                        ContactNo = employeeViewModel.ContactNo,
                        Email = employeeViewModel.Email,
                        Address = employeeViewModel.Address,
                        FilePath = path
                    };
                    employeeManager.Update(employee);
                    return RedirectToAction("Index");
                }
                else
                {
                    Employee employee = new Employee()
                    {
                        Id = employeeViewModel.Id,
                        Name = employeeViewModel.Name,
                        ContactNo = employeeViewModel.ContactNo,
                        Email = employeeViewModel.Email,
                        Address = employeeViewModel.Address,
                        FilePath = employeeViewModel.FilePath
                    };
                    employeeManager.Update(employee);
                    return RedirectToAction("Index");
                }
                
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var employee = employeeManager.GetById((int)id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            employeeManager.Remove(employee);
            return RedirectToAction("Index");
        }

        public ActionResult SingleEmployeeResult()
        {
            var userId = User.Identity.GetUserId();

            var employee = employeeManager.GetAll();
            var singleEmployee = employee.FirstOrDefault(c => c.UserId == userId);
            EmployeeViewModel employeeViewModels = new EmployeeViewModel()
            {
                Id = singleEmployee.Id,
                Name = singleEmployee.Name,
                ContactNo = singleEmployee.ContactNo,
                Email = singleEmployee.Email,
                Address = singleEmployee.Address,
                FilePath = singleEmployee.FilePath
            };
            return View(employeeViewModels);
        }
        
    }
}
