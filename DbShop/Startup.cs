﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DbShop.Startup))]
namespace DbShop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
