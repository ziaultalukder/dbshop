﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DbShop.ViewModel
{
    public class HomeProductViewModel
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ImagePath { get; set; }
        public decimal ProductPrice { get; set; }
    }
}