﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DbShop.Models.EntityModel;

namespace DbShop.ViewModel
{
    public class HomeViewModel
    {
        public int Id { get; set; }
        public int CatId { get; set; }
        public Category Category { get; set; }
        public List<Cart> CartItems { get; set; }
        public decimal CartTotal { get; set; }
        public IEnumerable<NewCollection> NewCollections1 { get; set; }
        public IEnumerable<NewCollection> NewCollections2 { get; set; }
        public IEnumerable<NewCollection> NewCollections3 { get; set; }
        public IEnumerable<NewCollection> NewCollections4 { get; set; }
        public IEnumerable<NewCollection> NewCollections5 { get; set; }
        public IEnumerable<NewCollection> NewCollections6 { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<CategoryViewModel> CategoryViewModels { get; set; }
        public IEnumerable<BannerImage> BannerImages { get; set; }
        public IEnumerable<HomeProductViewModel> HomeProductViewModels { get; set; }
    }
}