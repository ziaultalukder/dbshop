﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DbShop.ViewModel
{
    public class NewCollectionViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Image")]
        public string ImagePath { get; set; }
        public HttpPostedFileBase Image { get; set; }
    }
}