﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DbShop.Models.EntityModel;

namespace DbShop.ViewModel
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Display(Name = "Category")]
        public int? CatId { get; set; }
        public Category Cat { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}