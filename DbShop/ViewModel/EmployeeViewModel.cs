﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DbShop.ViewModel
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string FilePath { get; set; }
        public string UserId { get; set; }
        public HttpPostedFileBase Image { get; set; }
    }
}