﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DbShop.Models.EntityModel;

namespace DbShop.ViewModel
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public string FilePath { get; set; }
        public decimal Price { get; set; }
        public string UserName { get; set; }
        public string Details { get; set; }
        public IEnumerable<ProductImage> ProductImages { get; set; }
        [Display(Name = "Product Description")]
        public string Description { get; set; }

        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public List<HttpPostedFileBase> Image { get; set; }
    }
}