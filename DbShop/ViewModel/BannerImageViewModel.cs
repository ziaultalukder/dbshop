﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DbShop.ViewModel
{
    public class BannerImageViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Image")]
        public string ImagePath { get; set; }
        public string Title { get; set; }
        public decimal Discount { get; set; }
        public HttpPostedFileBase Image { get; set;}
    }
}