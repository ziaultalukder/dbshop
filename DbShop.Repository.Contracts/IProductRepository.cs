﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbShop.Models.EntityModel;

namespace DbShop.Repository.Contracts
{
    public interface IProductRepository:IRepository<Product>
    {
    }
}
