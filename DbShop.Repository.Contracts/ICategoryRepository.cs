﻿using DbShop.Models.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbShop.Repository.Contracts
{
    public interface ICategoryRepository:IRepository<Category>
    {
    }
}
