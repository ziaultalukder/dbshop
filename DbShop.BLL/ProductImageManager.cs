﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbShop.BLL.Base;
using DbShop.BLL.Contracts;
using DbShop.Models.EntityModel;
using DbShop.Repository.Contracts;

namespace DbShop.BLL
{
    public class ProductImageManager:Manager<ProductImage>,IProductImageManager
    {
        public IProductImageRepository ImageRepository;

        public ProductImageManager(IProductImageRepository Repository):base(Repository)
        {
            ImageRepository = Repository;
        }
    }
}
