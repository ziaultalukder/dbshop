namespace DbShop.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductTableUpdated2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Products", "FilePath");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "FilePath", c => c.String());
        }
    }
}
