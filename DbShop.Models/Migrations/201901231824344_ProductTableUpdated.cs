namespace DbShop.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductTableUpdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "UserName", c => c.String());
            AddColumn("dbo.Products", "Details", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Details");
            DropColumn("dbo.Products", "UserName");
        }
    }
}
