namespace DbShop.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ASPNET_User_Table_Changes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "FilePath", c => c.String());
            AddColumn("dbo.AspNetUsers", "FullName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "FullName");
            DropColumn("dbo.AspNetUsers", "FilePath");
        }
    }
}
