namespace DbShop.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewCollectionTableAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NewCollections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        ImagePath = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.NewCollections");
        }
    }
}
