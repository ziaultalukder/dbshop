namespace DbShop.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductImageTableUpdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductImages", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductImages", "IsDeleted");
        }
    }
}
