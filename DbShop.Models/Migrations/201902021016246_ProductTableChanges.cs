namespace DbShop.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductTableChanges : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Products", "NewPrice");
            DropColumn("dbo.Products", "Size");
            DropColumn("dbo.Products", "Color");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "Color", c => c.String());
            AddColumn("dbo.Products", "Size", c => c.String());
            AddColumn("dbo.Products", "NewPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
