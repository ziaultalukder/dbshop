﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbShop.Models.Contracts;

namespace DbShop.Models.EntityModel
{
    public class NewCollection:IModel,IDeletable
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public bool IsDeleted { get; set; }
        public bool WithDeleted()
        {
            return IsDeleted;
        }
    }
}
