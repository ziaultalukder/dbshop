﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbShop.Models.Contracts;

namespace DbShop.Models.EntityModel
{
    public class Category:IModel, IDeletable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CatId { get; set; }
        public Category Cat { get; set; }
        public bool IsDeleted { get; set; }
        public bool WithDeleted()
        {
            return IsDeleted;
        }
    }
}
