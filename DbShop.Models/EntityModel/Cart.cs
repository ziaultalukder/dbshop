﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbShop.Models.Contracts;

namespace DbShop.Models.EntityModel
{
    public class Cart: IDeletable
    {
        public int RecorId { get; set; }
        public string CartId { get; set; }
        public int Count { get; set; }
        public DateTime DateCreated { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public bool IsDeleted { get; set; }
        public bool WithDeleted()
        {
            return IsDeleted;
        }
    }
}
