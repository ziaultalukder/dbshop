using System.Data.Entity;
using DbShop.Models.EntityModel;
using DbShop.Models.IdentityModel;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DbShop.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<BannerImage> BannerImages { get; set; }
        public DbSet<NewCollection> NewCollections { get; set; }
        public DbSet<Cart> Carts { get; set; }
    }
}