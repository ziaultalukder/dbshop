﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbShop.Models.EntityModel;
using DbShop.Repository.Contracts;
using DbShop.Repository.Base;

namespace DbShop.Repository
{
    public class CategoryRepository:Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(DbContext _db) : base(_db)
        {
        }
    }
}
