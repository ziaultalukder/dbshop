﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbShop.Models.EntityModel;
using DbShop.Repository.Base;
using DbShop.Repository.Contracts;

namespace DbShop.Repository
{
    public class EmployeeRepository:Repository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(DbContext _db) : base(_db)
        {
        }
    }
}
